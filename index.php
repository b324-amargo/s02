<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<title>S02: Selection Control Structures and Array Manipulation</title>
</head>
<body>
	<h1>Divisibles of Five</h1>
		<p><?php displayDivisibleByFive(); ?></p>

	<h1>Array Manipulation</h1>
		<p><?php array_push($students, "John Smith"); ?></p>
		<p><?php print_r($students); ?></p>
		<p><?php echo count($students); ?></p>

		<p><?php array_push($students, "Jane Smith"); ?></p>
		<p><?php print_r($students); ?></p>
		<p><?php echo count($students); ?></p>

		<p><?php array_shift($students); ?></p>
		<p><?php print_r($students); ?></p>
		<p><?php echo count($students); ?></p>

</body>
</html>